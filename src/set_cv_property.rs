pub fn set_cv_property(client: &jack::Client) {
    let prop_sig_type = jack::Property::new(&"http://jackaudio.org/metadata/signal-type", None);
    if let Err(e) = client.property_set(client.uuid(), &"CV", &prop_sig_type) {
        println!("Couldn't set CV metadata, carrying on... {}", e);
    }
}
