extern crate libm;
extern crate jack;
extern crate arguably;

use arguably::ArgParser;

mod faust;
mod dsp;
mod set_cv_property;

use crate::dsp::*;

fn main() {
    let helptext = "cv-utils 0.1.0: simple utilities for jack control voltages.

Usage: cv-utils <utility>

For help with specific utilities, see cv-utils <utility> --help

Utilities: vca sample-hold track-hold slew-limiter intersect";

    let mut parser = ArgParser::new()
        .helptext(helptext)
        .version("0.1")
        .command("intersect", ArgParser::new()
            .helptext("Usage: cv-utils intersect <n_inputs>")
            .callback(start)
        )
        .command("sample-hold", ArgParser::new()
            .helptext("Usage: cv-utils sample-hold")
            .callback(start)
        )
        .command("slew-limiter", ArgParser::new()
            .helptext("Usage: cv-utils slew-limiter")
            .callback(start)
        )
        .command("track-hold", ArgParser::new()
            .helptext("Usage: cv-utils track-hold")
            .callback(start)
        )
        .command("vca", ArgParser::new()
            .helptext("Usage: cv-utils vca")
            .callback(start)
        );

    if let Err(err) = parser.parse() {
        err.exit();
    }

    if parser.cmd_name.is_none() {
        println!("{}", helptext);
    }
}

fn start(cmd_name: &str, parser: &ArgParser) {
    println!("cv-utils {} started", cmd_name);
    match cmd_name {
        "sample-hold" | "sample_hold" => { sample_hold::start() },
        "slew-limiter" | "slew_limiter" => { slew_limiter::start() },
        "track-hold" | "track_hold" => { track_hold::start() },
        "vca" => { vca::start() },
        "intersect" => {
            let n = if parser.args.len() > 0 {
                parser.args[0].parse::<usize>().unwrap_or(2)
            } else {
                2
            };
            intersect::start(n).unwrap()
        },
        _ =>  { }
    }
}
