/************************************************************************
************************************************************************
FAUST Architecture File
Copyright (C) 2017-2020 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------

This is sample code. This file is provided as an example of minimal
FAUST architecture file. Redistribution and use in source and binary
forms, with or without modification, in part or in full are permitted.
In particular you can create a derived work of this FAUST architecture
and distribute that work under terms of your choice.

This sample code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
************************************************************************
************************************************************************/

//! Faust JACK architecture file
use crate::faust::faust_dsp::*;
use crate::set_cv_property::set_cv_property;

pub struct Vca {
    f_sample_rate: i32,
}

impl FaustDsp for Vca {
    type T = f32;

    fn new() -> Vca {
        Vca {
            f_sample_rate: 0,
        }
    }
    fn metadata(&self, m: &mut dyn Meta) {
        m.declare("filename", "vca.dsp");
        m.declare("name", "vca");
    }

    fn get_sample_rate(&self) -> i32 {
        return self.f_sample_rate;
    }
    fn get_num_inputs(&self) -> i32 {
        return 2;
    }
    fn get_num_outputs(&self) -> i32 {
        return 1;
    }

    fn instance_constants(&mut self, sample_rate: i32) {
        self.f_sample_rate = sample_rate;
    }
    fn instance_init(&mut self, sample_rate: i32) {
        self.instance_constants(sample_rate);
        self.instance_reset_params();
        self.instance_clear();
    }
    fn init(&mut self, sample_rate: i32) {
        Vca::class_init(sample_rate);
        self.instance_init(sample_rate);
    }

    fn compute(&mut self, count: i32, inputs: &[&[Self::T]], outputs: &mut[&mut[Self::T]]) {
        let (inputs0, inputs1) = if let [inputs0, inputs1, ..] = inputs {
            let inputs0 = inputs0[..count as usize].iter();
            let inputs1 = inputs1[..count as usize].iter();
            (inputs0, inputs1)
        } else {
            panic!("wrong number of inputs");
        };
        let outputs0 = if let [outputs0, ..] = outputs {
            let outputs0 = outputs0[..count as usize].iter_mut();
            outputs0
        } else {
            panic!("wrong number of outputs");
        };
        let zipped_iterators = inputs0.zip(inputs1).zip(outputs0);
        for ((input0, input1), output0) in zipped_iterators {
            *output0 = ((*input0 as f32) * (*input1 as f32)) as f32;
        }
    }
}

pub fn start() {

    // Create JACK client
    let (client, _status) = jack::Client::new("vca", jack::ClientOptions::NO_START_SERVER).unwrap();

    // Allocation DSP on the heap
    let mut dsp = Box::new(Vca::new());

    // Init DSP with a given SR
    dsp.init(client.sample_rate() as i32);

    // Register ports. They will be used in a callback that will be
    // called when new data is available.

    let in_a = client.register_port("in-1", jack::AudioIn::default()).unwrap();
    let in_b = client.register_port("in-2", jack::AudioIn::default()).unwrap();

    let mut out_a = client.register_port("out", jack::AudioOut::default()).unwrap();

    let process_callback = move |client: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let input0: &[f32] = &in_a.as_slice(ps);
        let input1: &[f32] = &in_b.as_slice(ps);

        let output0: &mut[f32] = &mut out_a.as_mut_slice(ps);

        let inputs = &[input0, input1];
        let outputs = &mut[output0];

        dsp.compute(client.buffer_size() as i32, inputs, outputs);

        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let active_client = jack::AsyncClient::new(client, (), process).unwrap();
    set_cv_property(active_client.as_client());

    loop {
        std::thread::sleep(std::time::Duration::from_millis(200));
    }
}
