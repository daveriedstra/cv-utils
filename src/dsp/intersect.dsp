import("stdfaust.lib");

// We only need to compare this pair of samples against the last comparison
// and only trigger after the hysteresis period

n_streams = 4;
n_routing_outputs = n_streams * (n_streams - 1);
routing_matrix = par(x, n_streams-1, par(y, max(n_streams - x - 1, 1), make_routing(x, y))) with {
    make_routing(x, y) = (x+1, tally+1), (x+y+2, tally+2) with {
        /// have to do this bs because we can't just assign a variable to tally things like a normal language
        x_tier = n_streams * x - sum(z, x+1, z);
        tally = 2 * (x_tier + y);
    };
};
routing = route(n_streams, n_routing_outputs, routing_matrix);

hyst = 0.001; // 1ms rise, 1ms fall
gt = _ >= _;
new_cross = gt <: _ xor _';

process = routing : sum(i, n_routing_outputs / 2, new_cross) : >(0) : en.ar(hyst, hyst);
