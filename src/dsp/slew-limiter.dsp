import("stdfaust.lib");

// exponential slew limiting
// it would be good to have a linear option at some point
// possibility to expand to independent rise and fall times
// x0.25 multiplier is an estimate -- the builtin times are off for some reason
process = (abs : *(0.25) <: _, _), _ : si.lag_ud;
