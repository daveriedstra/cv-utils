/************************************************************************
************************************************************************
FAUST Architecture File
Copyright (C) 2017-2020 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------

This is sample code. This file is provided as an example of minimal
FAUST architecture file. Redistribution and use in source and binary
forms, with or without modification, in part or in full are permitted.
In particular you can create a derived work of this FAUST architecture
and distribute that work under terms of your choice.

This sample code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
************************************************************************
************************************************************************/

//! Faust JACK architecture file
use crate::faust::faust_dsp::*;
use crate::set_cv_property::set_cv_property;

pub struct Intersect {
    n_inputs: usize,
    n_comp: usize,
    i_vec: Vec<[i32;2]>,
    i_rec_0: [i32;2],
    f_sample_rate: i32,
    f_const_0: f32,
}

const MAX_N_INPUTS: usize = 12;

impl Intersect {
    fn make(n: usize) -> Intersect {
        let n_comp = n * (n-1) / 2; // should be the number of comparisons
        Intersect {
            n_inputs: n,
            n_comp,
            i_vec: vec![[0, 0]; n_comp + 1], // 1 extra for the has_crossing buffer
            i_rec_0: [0;2],
            f_sample_rate: 0,
            f_const_0: 0.0,
        }
    }

    fn compute_by_iter<'a, I>(&mut self, count: i32, inputs: I, outputs: &mut[&mut[f32]])
        where I: IntoIterator<Item = &'a[f32]>
    {
        // let empty = std::iter::empty::<&f32>() as Iterator<&f32>;
        let mut inputs = inputs.into_iter()
            .map(|i| i[..count as usize].iter()); // make sure the incoming buffers are the right length
            // then reduce from a list of buffers to an iterator that yields a tuple of each
            // buffer's sample
            // .reduce(|a: dyn IntoIterator, b| {
            // .fold(empty, |a, b| {
                // a.into_iter()
                //     .zip(b)     // ((-a, a), b)
                //     .flatten()  // (-a, a, b)
            // });
        // let (inputs0, inputs1) = if let [inputs0, inputs1, ..] = inputs {
        //     let inputs0 = inputs0[..count as usize].iter();
        //     let inputs1 = inputs1[..count as usize].iter();
        //     (inputs0, inputs1)
        // } else {
        //     panic!("wrong number of inputs");
        // };
        let outputs0 = if let [outputs0, ..] = outputs {
            outputs0[..count as usize].iter_mut()
        } else {
            panic!("wrong number of outputs");
        };

        // collect into vec (heap alloc!)
        // let mut input_samps = Vec::with_capacity(self.n_inputs);
        // let input_samps: Vec<&f32> = (0..self.n_inputs).map(|_| {
        //     if let Some(mut input) = inputs.next() {
        //         input.next().unwrap_or(&0.)
        //     } else {
        //         &0.
        //     }
        // }).collect();

        // "collect" into array (stack alloc but more than needed -- might be a thought to allocate
        // this out of the loop, except then it would need to be re-zeroed each loop... though
        // actually if we always loop until MAX_N_INPUTS we can do that at the same time)
        let mut input_samps: [&f32; MAX_N_INPUTS] = [&0.; MAX_N_INPUTS];
        (0..MAX_N_INPUTS).for_each(|i| {
            let mut samp = &0.;
            if let Some(mut input) = inputs.next() {
                samp = input.next().unwrap_or(&0.);
            }
            input_samps[i] = samp;
        });

        // let mut input_samps = [0_f32; self.n_inputs];
        for output0 in outputs0 {
            // compare all inputs:
            // compare i against (i+j+1) and store in k
            let mut k = 0;
            for i in 0..(self.n_inputs - 1) {
                for j in 0..(self.n_inputs - i - 1) {
                    self.i_vec[k][0] = (*input_samps[i] >= *input_samps[i + j + 1]) as i32;
                    k += 1;
                }
            }

            // XOR each input against its one-sample delay
            let has_crossing: i32 = self.i_vec.iter()
                .fold(0, |acc, i_vec| {
                    acc + (i_vec[0] ^ i_vec[1])
                });
            let has_crossing = (has_crossing > 0) as i32;
            let xing_index = k;
            self.i_vec[xing_index][0] = has_crossing;

            // generate & render envelope
            self.i_rec_0[0] = ((self.i_rec_0[1] + ((self.i_rec_0[1] > 0) as i32)) * ((has_crossing <= self.i_vec[xing_index][1]) as i32)) + ((has_crossing > self.i_vec[xing_index][1]) as i32);
            let f_temp_2: f32 = self.f_const_0 * (self.i_rec_0[0] as f32);
            *output0 = f32::max(0.0, f32::min(f_temp_2, 2.0 - f_temp_2)) as f32;

            // apply one-sample delay
            for i in 0..(self.n_comp + 1) {
                self.i_vec[i][1] = self.i_vec[i][0];
            }
            self.i_rec_0[1] = self.i_rec_0[0];

            // original code

            // let i_temp_0: i32 = ((*input_samps[0] as f32) >= (*input_samps[1] as f32)) as i32;
            // self.i_vec[0][0] = i_temp_0;

            // let i_temp_1: i32 = i_temp_0 ^ self.i_vec_0[1];
            // self.i_vec[1][0] = i_temp_1;

            // self.i_rec_0[0] = ((self.i_rec_0[1] + ((self.i_rec_0[1] > 0) as i32)) * ((i_temp_1 <= self.i_vec[1][1]) as i32)) + ((i_temp_1 > self.i_vec[1][1]) as i32);
            // let f_temp_2: f32 = self.f_const_0 * (self.i_rec_0[0] as f32);
            // *output0 = f32::max(0.0, f32::min(f_temp_2, 2.0 - f_temp_2)) as f32;
            // self.i_vec[0][1] = self.i_vec[0][0];
            // self.i_vec[1][1] = self.i_vec[1][0];
            // self.i_rec_0[1] = self.i_rec_0[0];

            // // let input_a: f32 = *input0 as f32;
            // // let input_b: f32 = *input1 as f32;
            // // // let is_new_crossing = is_new_crossing && !is_hyst;

            // // self.f_vec_0[0] = input_a;
            // // let i_temp_1: i32 = ((self.f_vec_0[1] <= 0.0) as i32) & ((input_a > 0.0) as i32);
            // // self.f_rec_0[0] = (self.f_rec_0[1] * ((1 - i_temp_1) as f32)) + ((*input1 as f32) * (i_temp_1 as f32));
            // // *output0 = self.f_rec_0[0] as f32;
            // // self.f_vec_0[1] = self.f_vec_0[0];
            // // self.f_rec_0[1] = self.f_rec_0[0];
        }
    }

    pub fn set_n_inputs(&mut self, n: usize) -> Result<(), String> {
        if n > MAX_N_INPUTS {
            let msg = format!("Requested number of inputs ({}) is greater than the maximum ({})", n, MAX_N_INPUTS);
            return Err(msg);
        } else {
            self.n_inputs = n;
            self.instance_init(self.f_sample_rate);
            Ok(())
        }
    }
}

impl FaustDsp for Intersect {
    type T = f32;

    fn new() -> Intersect {
        Self::make(2)
    }

    fn metadata(&self, m: &mut dyn Meta) {
        m.declare("basics.lib/name", "Faust Basic Element Library");
        m.declare("basics.lib/version", "0.2");
        m.declare("filename", "intersect.dsp");
        m.declare("name", "intersect");
    }

    fn get_sample_rate(&self) -> i32 {
        return self.f_sample_rate;
    }
    fn get_num_inputs(&self) -> i32 {
        return self.n_inputs as i32;
    }
    fn get_num_outputs(&self) -> i32 {
        return 1;
    }

    fn instance_clear(&mut self) {
        for i_vec in self.i_vec.iter_mut() {
            for l0 in 0..2 {
                i_vec[l0] = 0;
            }
        }
        for l2 in 0..2 {
            self.i_rec_0[l2 as usize] = 0;
        }
    }

    fn instance_constants(&mut self, sample_rate: i32) {
        self.f_sample_rate = sample_rate;
        self.f_const_0 = 1.0 / f32::max(1.0, 0.00100000005 * f32::min(192000.0, f32::max(1.0, self.f_sample_rate as f32)));
    }

    fn instance_init(&mut self, sample_rate: i32) {
        self.instance_constants(sample_rate);
        self.instance_reset_params();
        self.instance_clear();
    }

    fn init(&mut self, sample_rate: i32) {
        Intersect::class_init(sample_rate);
        self.instance_init(sample_rate);
    }

    fn compute(&mut self, _count: i32, _inputs: &[&[Self::T]], _outputs: &mut[&mut[Self::T]]) {
    }
}


pub fn start(n_inputs: usize) -> Result<(), String> {
    // Create JACK client
    let (client, _status) = jack::Client::new("intersect", jack::ClientOptions::NO_START_SERVER).unwrap();

    // Allocation DSP on the heap
    let mut dsp = Box::new(Intersect::make(n_inputs));
    dsp.set_n_inputs(n_inputs)?;

    // Init DSP with a given SR
    dsp.init(client.sample_rate() as i32);

    // Register ports. They will be used in a callback that will be
    // called when new data is available.

    let input_ports: Vec<_> = (0..n_inputs).map(|i| {
        client.register_port(&format!("input-{}", i), jack::AudioIn::default()).unwrap()
    }).collect();

    let mut out_a = client.register_port("out", jack::AudioOut::default()).unwrap();

    let process_callback = move |client: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let in_buffs = input_ports.iter()
            .take(n_inputs)
            .map(|p| p.as_slice(ps));

        let output0: &mut[f32] = &mut out_a.as_mut_slice(ps);
        let outputs = &mut[output0];

        dsp.compute_by_iter(client.buffer_size() as i32, in_buffs, outputs);

        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let active_client = jack::AsyncClient::new(client, (), process).unwrap();
    set_cv_property(active_client.as_client());

    loop {
        std::thread::sleep(std::time::Duration::from_millis(200));
    }
}
