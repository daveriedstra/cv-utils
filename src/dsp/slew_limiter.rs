/************************************************************************
************************************************************************
FAUST Architecture File
Copyright (C) 2017-2020 GRAME, Centre National de Creation Musicale
---------------------------------------------------------------------

This is sample code. This file is provided as an example of minimal
FAUST architecture file. Redistribution and use in source and binary
forms, with or without modification, in part or in full are permitted.
In particular you can create a derived work of this FAUST architecture
and distribute that work under terms of your choice.

This sample code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
************************************************************************
************************************************************************/

//! Faust JACK architecture file
use crate::faust::faust_dsp::*;
use crate::set_cv_property::set_cv_property;

pub struct SlewLimiter {
    f_sample_rate: i32,
    f_const_0: f32,
    f_rec_1: [f32;2],
}

impl FaustDsp for SlewLimiter {
    type T = f32;

    fn new() -> SlewLimiter {
        SlewLimiter {
            f_sample_rate: 0,
            f_const_0: 0.0,
            f_rec_1: [0.0;2],
        }
    }
    fn metadata(&self, m: &mut dyn Meta) {
        m.declare("basics.lib/name", "Faust Basic Element Library");
        m.declare("basics.lib/version", "0.2");
        m.declare("filename", "slew-limiter.dsp");
        m.declare("maths.lib/author", "GRAME");
        m.declare("maths.lib/copyright", "GRAME");
        m.declare("maths.lib/license", "LGPL with exception");
        m.declare("maths.lib/name", "Faust Math Library");
        m.declare("maths.lib/version", "2.5");
        m.declare("name", "slew-limiter");
        m.declare("platform.lib/name", "Generic Platform Library");
        m.declare("platform.lib/version", "0.2");
        m.declare("signals.lib/name", "Faust Signal Routing Library");
        m.declare("signals.lib/version", "0.1");
    }

    fn get_sample_rate(&self) -> i32 {
        return self.f_sample_rate;
    }
    fn get_num_inputs(&self) -> i32 {
        return 2;
    }
    fn get_num_outputs(&self) -> i32 {
        return 1;
    }

    fn instance_clear(&mut self) {
        for l0 in 0..2 {
            self.f_rec_1[l0 as usize] = 0.0;
        }
    }
    fn instance_constants(&mut self, sample_rate: i32) {
        self.f_sample_rate = sample_rate;
        self.f_const_0 = 1.0 / f32::min(192000.0, f32::max(1.0, self.f_sample_rate as f32));
    }
    fn instance_init(&mut self, sample_rate: i32) {
        self.instance_constants(sample_rate);
        self.instance_reset_params();
        self.instance_clear();
    }
    fn init(&mut self, sample_rate: i32) {
        SlewLimiter::class_init(sample_rate);
        self.instance_init(sample_rate);
    }

    fn compute(&mut self, count: i32, inputs: &[&[Self::T]], outputs: &mut[&mut[Self::T]]) {
        let (inputs0, inputs1) = if let [inputs0, inputs1, ..] = inputs {
            let inputs0 = inputs0[..count as usize].iter();
            let inputs1 = inputs1[..count as usize].iter();
            (inputs0, inputs1)
        } else {
            panic!("wrong number of inputs");
        };
        let outputs0 = if let [outputs0, ..] = outputs {
            let outputs0 = outputs0[..count as usize].iter_mut();
            outputs0
        } else {
            panic!("wrong number of outputs");
        };
        let zipped_iterators = inputs0.zip(inputs1).zip(outputs0);
        for ((input0, input1), output0) in zipped_iterators {
            let f_temp_0: f32 = 0.25 * f32::abs(*input0 as f32);
            let i_temp_1: i32 = (f32::abs(f_temp_0) < 1.1920929e-07) as i32;
            let f_temp_2: f32 = if i_temp_1 as i32 != 0 {
                0.0
            } else {
                f32::exp(0.0 - (self.f_const_0 / if i_temp_1 as i32 != 0 { 1.0 } else { f_temp_0 }))
            };
            self.f_rec_1[0] = (self.f_rec_1[1] * f_temp_2) + ((*input1 as f32) * (1.0 - f_temp_2));
            let f_rec_0: f32 = self.f_rec_1[0];
            *output0 = f_rec_0 as f32;
            self.f_rec_1[1] = self.f_rec_1[0];
        }
    }

}

pub fn start() {

    // Create JACK client
    let (client, _status) = jack::Client::new("slew-limiter", jack::ClientOptions::NO_START_SERVER).unwrap();

    // Allocation DSP on the heap
    let mut dsp = Box::new(SlewLimiter::new());

    // Init DSP with a given SR
    dsp.init(client.sample_rate() as i32);

    // Register ports. They will be used in a callback that will be
    // called when new data is available.

    let in_time = client.register_port("slew-time", jack::AudioIn::default()).unwrap();
    let in_cv = client.register_port("cv", jack::AudioIn::default()).unwrap();

    let mut out_a = client.register_port("out", jack::AudioOut::default()).unwrap();

    let process_callback = move |client: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
        let input0: &[f32] = &in_time.as_slice(ps);
        let input1: &[f32] = &in_cv.as_slice(ps);

        let output0: &mut[f32] = &mut out_a.as_mut_slice(ps);

        let inputs = &[input0, input1];
        let outputs = &mut[output0];

        dsp.compute(client.buffer_size() as i32, inputs, outputs);

        jack::Control::Continue
    };
    let process = jack::ClosureProcessHandler::new(process_callback);

    // Activate the client, which starts the processing.
    let active_client = jack::AsyncClient::new(client, (), process).unwrap();
    set_cv_property(active_client.as_client());

    loop {
        std::thread::sleep(std::time::Duration::from_millis(200));
    }
}
