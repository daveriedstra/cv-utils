# cv-utils

Simple utilities for jack control voltages.

## Usage

```
cv-utils <utility>
```

Available utilities:

* `vca`
* `slew-limiter` (exponential)
* `sample-hold`
* `track-hold`
* `intersect` (output triggers on input signal crossings)

Planned utilities:

* pitch quantizer? (Need to know voltage scale of output)
* clock divider

## About

Sometimes you just need something quick & dirty to scale down some CV and you don't want to open a whole GUI program just for one silly little `[*~]`.

These utilities were developed in FAUST and the original FAUST code is included in the `src/dsp` directory. I then removed some cruft, updated the jack library, and wrapped it up in a cozy little CLI.

## Installation

This is a rust project, so you'll need [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html). You'll also need the jack development libraries (look for something like `libjack` or `libjack-jackd2-0` with your package manager or install from the [website](http://jackaudio.org/downloads/) if you're on Windows). Once those are installed, you can simply

```
cargo install --git https://gitlab.com/daveriedstra/cv-utils
```

This was developed on Linux. Theoretically it should work on Mac or Windows but it's untested.
